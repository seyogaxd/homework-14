function filterBy(arr, type) {
    let newarr = [];
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] !== type) {
            newarr.push(arr[i]);
        }
    }
    return newarr;
}
let arr = ['hello', 'world', 23, '23', null];
let type = "string";
console.log(filterBy(arr, type));